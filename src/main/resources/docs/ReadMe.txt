Hi,

Please follow below instruction to run the employee portal.

 Bring rest webservice up.

1) git clone https://ankitpradhan1987@bitbucket.org/ankitpradhan1987/employee.git
2) import in Intellijidea and run or do "mvn spring-boot:run" from root directory of project "employee".

Bring React UI up

3) git clone https://ankitpradhan1987@bitbucket.org/ankitpradhan1987/ems-ui.git
4) install npm and node on your machine.
5) execute "npm install" from root directory "EMS-UI".
6) execute "npm start" from root directory "EMS-UI".

Note: Server side has below layers.

1) Controller --> 2) Service Layer --> 3) Dao Layer (Static and MySql Implementation) -> 4) Mapper Layer.

Note: Dao layer has 2 implementation and can be injected in service layer "EmployeeServiceImpl"
by using @Qualifier. default is static dao.

    a) EmployeeDaoStaticImpl @Qualifier("staticDao")
    b) EmployeeDaoImpl       @Qualifier("mysqlDao")

Default mode is static. You can play around with it. If you wish to switch to mysql you can go to
application.properties for mysql database details. and create Employee table and insert data using below scripts

    a) Create_Employee.sql
    b) Insert_EMployee.sql

These scripts are checked in resources/db-scripts.

