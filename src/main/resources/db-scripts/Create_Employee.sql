CREATE TABLE EMPLOYEE (id VARCHAR(32) NOT NULL PRIMARY KEY,
employee_id VARCHAR(32) NOT NULL,

first_name VARCHAR(20),
last_name VARCHAR(20),
phone_number VARCHAR(20),
hire_date TIMESTAMP,
salary INT,

active CHAR(1) NOT NULL ,
created_by VARCHAR(32) NOT NULL,
last_updated_by VARCHAR(32) NOT NULL,
create_date TIMESTAMP,
last_update_date TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

select * from employee;