package com.learningpoint.portal.employee.dao.intf;

import com.learningpoint.portal.employee.pojo.Employee;

import java.util.List;

public interface EmployeeDaoIntf {
    List<Employee> getEmployees(int pageNum, int pageSize, String sortBy, String order);

    Employee getEmployee(String employeeId);
}
