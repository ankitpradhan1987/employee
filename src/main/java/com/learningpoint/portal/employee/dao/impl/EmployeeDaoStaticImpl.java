package com.learningpoint.portal.employee.dao.impl;

import com.learningpoint.portal.employee.dao.intf.EmployeeDaoIntf;
import com.learningpoint.portal.employee.pojo.Employee;
import com.learningpoint.portal.employee.utils.EmployeeUtil;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
@Qualifier("staticDao")
public class EmployeeDaoStaticImpl implements EmployeeDaoIntf {

    private static List<Employee> employees;
    private static Map<String, Employee> employeeMap;
    private static Map<String, Comparator<Employee>> comparatorMap;

    static {
        comparatorMap = new HashMap<>();
        Comparator<Employee> firstNameComparator = Comparator.comparing(Employee::getFirstName);
        Comparator<Employee> lastNameComparator = Comparator.comparing(Employee::getLastName);
        Comparator<Employee> salaryComparator = Comparator.comparing(Employee::getSalary);
        Comparator<Employee> hireDateComparator = Comparator.comparing(Employee::getHireDate);

        comparatorMap.put("firstName", firstNameComparator);
        comparatorMap.put("lastName", lastNameComparator);
        comparatorMap.put("salary", salaryComparator);
        comparatorMap.put("hireDate", hireDateComparator);
        comparatorMap.put("null", firstNameComparator);

        employees =  Stream.of(new Employee("id1","empid1","Ankit","Pradhan",13000d, EmployeeUtil.getDate("13/01/2019"),"9819261485"),
                new Employee("id2","empid2","Lankit","Apradhan",12000d, EmployeeUtil.getDate("12/01/2019"),"9819261485"),
                new Employee("id3","empid3","Kankit","Bpradhan", 11000d, EmployeeUtil.getDate("11/01/2019"),"9819261485"),
                new Employee("id4","empid4","Jankit","Cpradhan",10000d, EmployeeUtil.getDate("10/01/2019"),"9819261485"),
                new Employee("id5","empid5","Iankit","Dpradhan",9000d, EmployeeUtil.getDate("09/01/2019"),"9819261485"),
                new Employee("id6","empid6","Hankit","Epradhan",8000d, EmployeeUtil.getDate("08/01/2019"),"9819261485"),
                new Employee("id7","empid7","Gankit","Fpradhan",7000d, EmployeeUtil.getDate("07/01/2019"),"9819261485"),
                new Employee("id8","empid8","Fankit","Gpradhan",6000d, EmployeeUtil.getDate("06/01/2019"),"9819261485"),
                new Employee("id9","empid9","Eankit","Hpradhan",5000d, EmployeeUtil.getDate("05/01/2019"),"9819261485"),
                new Employee("id10","empid10","Dankit","Ipradhan",4000d, EmployeeUtil.getDate("04/01/2019"),"9819261485"),
                new Employee("id11","empid12","Cankit","Jpradhan",3000d, EmployeeUtil.getDate("03/01/2019"),"9819261485"),
                new Employee("id12","empid13","Bankit","Kpradhan",2000d, EmployeeUtil.getDate("02/01/2019"),"9819261485"),
                new Employee("id13","empid14","Aankit","Lpradhan",1000d, EmployeeUtil.getDate("01/01/2019"),"9819261485")
        )
                .collect(Collectors.toList());

        employeeMap = new HashMap<>();
        employees.forEach(employee -> employeeMap.put(employee.getEmployeeId(), employee));
    }

    @Override
    public List<Employee> getEmployees(int pageNum, int pageSize, String sortBy, String order) {

        List<Employee> employeesTemp = new ArrayList<>();
        int startIndex;
        int endIndex ;
        int employeeCount = employees.size();

        employees.sort(comparatorMap.get(sortBy));

        startIndex = (pageNum-1)*pageSize;
        endIndex = startIndex + pageSize;
        endIndex = endIndex > employeeCount ? employeeCount: endIndex+1;
        if(endIndex > employeeCount){
            endIndex--;
        }

        if(startIndex > employeeCount){
            return employeesTemp;
        }
        employeesTemp = employees.subList(startIndex , endIndex);
        return employeesTemp;
    }

    @Override
    public Employee getEmployee(String employeeId) {
        return employeeMap.get(employeeId);
    }
}
