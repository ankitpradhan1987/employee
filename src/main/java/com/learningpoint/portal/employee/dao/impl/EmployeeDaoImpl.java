package com.learningpoint.portal.employee.dao.impl;

import com.learningpoint.portal.employee.dao.intf.EmployeeDaoIntf;
import com.learningpoint.portal.employee.mapper.EmployeeMapper;
import com.learningpoint.portal.employee.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("mysqlDao")
public class EmployeeDaoImpl implements EmployeeDaoIntf {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Override
    public List<Employee> getEmployees(int pageNum, int pageSize, String sortBy, String order) {
        return employeeMapper.getEmployees((pageNum-1) * pageSize, pageSize+1, sortBy, order);
    }

    @Override
    public Employee getEmployee(String employeeId) {
        return employeeMapper.getEmployee(employeeId);
    }
}
