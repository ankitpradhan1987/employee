package com.learningpoint.portal.employee.mapper;

import com.learningpoint.portal.employee.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface EmployeeMapper {

    List<Employee> getEmployees(@Param("offset") int offset, @Param("recordCount") int recordCount,
                                @Param("sortBy") String sortBy, @Param("order")String order);

    Employee getEmployee(@Param("employeeId") String employeeId);
}
