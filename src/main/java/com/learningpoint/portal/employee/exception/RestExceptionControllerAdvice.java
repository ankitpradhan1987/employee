package com.learningpoint.portal.employee.exception;

import com.learningpoint.portal.employee.pojo.http.ExceptionConstants;
import com.learningpoint.portal.employee.pojo.http.ExceptionEntity;
import com.learningpoint.portal.employee.utils.EmployeeConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Objects;
import java.util.Set;

@ControllerAdvice
public class RestExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(RestExceptionControllerAdvice.class);

    @ExceptionHandler(value = ValidationException.class)
    protected ResponseEntity<ExceptionEntity> handleValidationException(ValidationException ve, WebRequest request){
        log.error("Inside RestExceptionControllerAdvice handleValidationException message is "
                +ve.getMessage()+" Cause is "+ve.getCause()+ve);
        ExceptionEntity entity = new ExceptionEntity(HttpStatus.BAD_REQUEST.getReasonPhrase(),
                ve.getAppErrorCode(),
                ve.getAppErrorMessage(),
                ve.getEntityProperty()+ EmployeeConstant.COLON_SEPARATOR.getValue()
                        + ve.getAppErrorMessage(),
                ve.getLocalizedMessage(),
                ve.getMessage());
        return new ResponseEntity<>(entity,new HttpHeaders(),
                HttpStatus.BAD_REQUEST );

    }


    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<ExceptionEntity> handleEntityNotFoundException(EntityNotFoundException enf,
                                                                         WebRequest request){
        log.error("Inside RestExceptionControllerAdvice handleEntityNotFoundException message is "
                +enf.getMessage()+" Cause is "+enf.getCause()+enf);
        ExceptionEntity entity = new ExceptionEntity(HttpStatus.NOT_FOUND.getReasonPhrase(),
                enf.getAppErrorCode(),
                enf.getAppErrorMessage(),
                enf.getAppErrorMessage(),
                enf.getLocalizedMessage(),
                enf.getMessage());
        return new ResponseEntity<>(entity,new HttpHeaders(),
                HttpStatus.NOT_FOUND );

    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public  ResponseEntity<ExceptionEntity> handleConstraintViolationException(Exception ex, WebRequest request){
        log.error("Inside RestExceptionControllerAdvice handleGenericException message is "
                +ex.getMessage()+" Cause is "+ex.getCause()+ex);
        ExceptionEntity entity = new ExceptionEntity(HttpStatus.FORBIDDEN.getReasonPhrase(),
                ExceptionConstants.INVALID_REQUEST_PARAMETER_FORMAT.getAppErrorCode(),
                ExceptionConstants.INVALID_REQUEST_PARAMETER_FORMAT.getAppErrorMessage(),
                HttpStatus.FORBIDDEN.getReasonPhrase(),
                ex.getLocalizedMessage(),
                ex.getMessage());
        return new ResponseEntity<>(entity,new HttpHeaders(),
                HttpStatus.FORBIDDEN );

    }

    @ExceptionHandler(value = Exception.class)
    public  ResponseEntity<ExceptionEntity> handleGenericException(Exception ex, WebRequest request){
        log.error("Inside RestExceptionControllerAdvice handleGenericException message is "
                +ex.getMessage()+" Cause is "+ex.getCause()+ex);
        ExceptionEntity entity = new ExceptionEntity(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                ExceptionConstants.A01.getAppErrorCode(),
                ExceptionConstants.A01.getAppErrorMessage(),
                HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                ex.getLocalizedMessage(),
                ex.getMessage());
        return new ResponseEntity<>(entity,new HttpHeaders(),
                HttpStatus.INTERNAL_SERVER_ERROR );

    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ExceptionEntity entity = new ExceptionEntity(HttpStatus.NOT_FOUND.getReasonPhrase(),
                ExceptionConstants.PAGE_NOT_FOUND.getAppErrorCode(),
                ExceptionConstants.PAGE_NOT_FOUND.getAppErrorMessage(),
                HttpStatus.NOT_FOUND.getReasonPhrase(),
                ex.getLocalizedMessage(),
                ex.getMessage());
        return this.handleExceptionInternal(ex,entity, headers, status, request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        pageNotFoundLogger.warn(ex.getMessage());
        Set<HttpMethod> supportedMethods = ex.getSupportedHttpMethods();
        if (!Objects.requireNonNull(supportedMethods).isEmpty()) {
            headers.setAllow(supportedMethods);
        }
        ExceptionEntity entity = new ExceptionEntity(HttpStatus.NOT_FOUND.getReasonPhrase(),
                ExceptionConstants.PAGE_NOT_FOUND.getAppErrorCode(),
                ExceptionConstants.PAGE_NOT_FOUND.getAppErrorMessage(),
                HttpStatus.NOT_FOUND.getReasonPhrase(),
                ex.getLocalizedMessage(),
                ex.getMessage());
        return this.handleExceptionInternal(ex, entity, headers, status, request);
    }


    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        ExceptionEntity entity = new ExceptionEntity(HttpStatus.BAD_REQUEST.getReasonPhrase(),
                ExceptionConstants.MISSING_REQUEST_PARAMETER.getAppErrorCode(),
                ExceptionConstants.MISSING_REQUEST_PARAMETER.getAppErrorMessage(),
                HttpStatus.BAD_REQUEST.getReasonPhrase(),
                ex.getLocalizedMessage(),
                ex.getMessage());
        return this.handleExceptionInternal(ex, entity, headers, status, request);
    }

    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        ExceptionEntity entity = new ExceptionEntity(HttpStatus.BAD_REQUEST.getReasonPhrase(),
                ExceptionConstants.INVALID_REQUEST_PARAMETER_FORMAT.getAppErrorCode(),
                ExceptionConstants.INVALID_REQUEST_PARAMETER_FORMAT.getAppErrorMessage(),
                HttpStatus.BAD_REQUEST.getReasonPhrase(),
                ex.getLocalizedMessage(),
                ex.getMessage());
        return this.handleExceptionInternal(ex, entity, headers, status, request);
    }
}
