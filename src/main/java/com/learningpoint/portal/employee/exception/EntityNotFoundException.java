package com.learningpoint.portal.employee.exception;

public class EntityNotFoundException extends RuntimeException {

    private String searchKey;
    private String appErrorCode;
    private String appErrorMessage;

    public EntityNotFoundException(String searchKey, String appErrorCode, String appErrorMessage) {
        this.searchKey = searchKey;
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
    }

    public EntityNotFoundException(String message, Throwable cause, String searchKey, String appErrorCode, String appErrorMessage) {
        super(message, cause);
        this.searchKey = searchKey;
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
    }

    public String getSearchKey() {
        return searchKey;
    }

    public String getAppErrorCode() {
        return appErrorCode;
    }

    public String getAppErrorMessage() {
        return appErrorMessage;
    }

    @Override
    public String toString() {
        return "EntityNotFoundException{" +
                "searchKey='" + searchKey + '\'' +
                ", appErrorCode='" + appErrorCode + '\'' +
                ", appErrorMessage='" + appErrorMessage + '\'' +
                '}';
    }
}
