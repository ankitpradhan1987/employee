package com.learningpoint.portal.employee.exception;

public class ValidationException extends RuntimeException {

    private String entityProperty;
    private String appErrorCode;
    private String appErrorMessage;

    public ValidationException(String entityProperty, String appErrorCode, String appErrorMessage) {
        this.entityProperty = entityProperty;
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
    }

    public ValidationException(String message, Throwable cause, String entityProperty, String appErrorCode, String appErrorMessage) {
        super(message, cause);
        this.entityProperty = entityProperty;
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
    }

    public String getEntityProperty() {
        return entityProperty;
    }

    public String getAppErrorCode() {
        return appErrorCode;
    }

    public String getAppErrorMessage() {
        return appErrorMessage;
    }

    @Override
    public String toString() {
        return "ValidationException{" +
                "entityProperty='" + entityProperty + '\'' +
                ", appErrorCode='" + appErrorCode + '\'' +
                ", appErrorMessage='" + appErrorMessage + '\'' +
                '}';
    }
}
