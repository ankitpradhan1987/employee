package com.learningpoint.portal.employee.pojo.http;

public enum ExceptionConstants {

    A01("Internal Server Error","Unknkown Error"),

    EMPLOYEE_NOT_FOUND("NF01","Employee not found Exception"),
    PAGE_NOT_FOUND("NF02", "Page not found Exception"),

    EMPLOYEE_NOT_DELETED("ND01","Employee not deleted Exception"),

    EMPLOYEE_NOT_CREATED("NC01","Exception while creating employee"),

    EMPLOYEE_NOT_UPDATED("NU01","Employee not updated Exception"),

    AUTHENTICATION_ERROR("CE01","Authentication Error"),
    WORK_IN_PROGRESS("CE02","Sorry work in progress"),

    PROPERTY_BLANK_VALIDATION("VE01","Property can not be blank"),
    MISSING_REQUEST_PARAMETER("VE02","Missing request parameter Exception"),
    INVALID_REQUEST_PARAMETER_FORMAT("VE03","Invalid request parameter format Exception"),
    INVALID_REQUEST_SORT_BY_PARAMETER_FORMAT("VE04","Invalid request parameter sortBy format Exception"),
    INVALID_REQUEST_ORDER_PARAMETER_FORMAT("VE05","Invalid request parameter order format Exception");

    private String appErrorCode;
    private String appErrorMessage;

    ExceptionConstants(String appErrorCode, String appErrorMessage) {
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
    }

    public String getAppErrorCode() {
        return appErrorCode;
    }

    public String getAppErrorMessage() {
        return appErrorMessage;
    }
}
