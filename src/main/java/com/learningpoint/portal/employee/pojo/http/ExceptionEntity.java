package com.learningpoint.portal.employee.pojo.http;

public class ExceptionEntity {
    private String status;
    private String appErrorCode;
    private String appErrorMessage;
    private String developerMessage;
    private String exceptionCause;
    private String exceptionMessage;

    public ExceptionEntity(String status, String appErrorCode, String appErrorMessage) {
        this.status = status;
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
    }

    public ExceptionEntity(String status, String appErrorCode, String appErrorMessage, String developerMessage) {
        this.status = status;
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
        this.developerMessage = developerMessage;
    }

    public ExceptionEntity(String status, String appErrorCode, String appErrorMessage, String developerMessage, String exceptionCause, String exceptionMessage) {
        this.status = status;
        this.appErrorCode = appErrorCode;
        this.appErrorMessage = appErrorMessage;
        this.developerMessage = developerMessage;
        this.exceptionCause = exceptionCause;
        this.exceptionMessage = exceptionMessage;
    }

    public String getStatus() {
        return status;
    }

    public String getAppErrorCode() {
        return appErrorCode;
    }

    public String getAppErrorMessage() {
        return appErrorMessage;
    }

    public String getDeveloperMessage() {
        return developerMessage;
    }

    public String getExceptionCause() {
        return exceptionCause;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }
}
