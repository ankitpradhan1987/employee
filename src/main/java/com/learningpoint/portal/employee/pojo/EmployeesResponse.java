package com.learningpoint.portal.employee.pojo;

import java.util.List;

public class EmployeesResponse {

    private List<Employee> employees;
    private Boolean hasMoreRecords;

    public EmployeesResponse(List<Employee> employees, Boolean hasMoreRecords) {
        this.employees = employees;
        this.hasMoreRecords = hasMoreRecords;
    }

    public EmployeesResponse() {
    }

    public EmployeesResponse(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public Boolean getHasMoreRecords() {
        return hasMoreRecords;
    }

    public void setHasMoreRecords(Boolean hasMoreRecords) {
        this.hasMoreRecords = hasMoreRecords;
    }

    @Override
    public String toString() {
        return "EmployeesResponse{" +
                "employees=" + employees +
                ", hasMoreRecords=" + hasMoreRecords +
                '}';
    }
}
