package com.learningpoint.portal.employee.service.intf;

import com.learningpoint.portal.employee.pojo.Employee;
import com.learningpoint.portal.employee.pojo.EmployeesResponse;

public interface EmployeeServiceIntf {
    EmployeesResponse getEmployees(int pageNum, int pageSize, String sortBy, String order);

    Employee getEmployees(String employeeId);
}
