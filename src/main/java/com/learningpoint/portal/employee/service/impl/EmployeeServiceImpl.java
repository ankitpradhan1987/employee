package com.learningpoint.portal.employee.service.impl;

import com.learningpoint.portal.employee.dao.intf.EmployeeDaoIntf;
import com.learningpoint.portal.employee.exception.EntityNotFoundException;
import com.learningpoint.portal.employee.pojo.Employee;
import com.learningpoint.portal.employee.pojo.EmployeesResponse;
import com.learningpoint.portal.employee.pojo.http.ExceptionConstants;
import com.learningpoint.portal.employee.service.intf.EmployeeServiceIntf;
import com.learningpoint.portal.employee.utils.EmployeeConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeServiceIntf {

    @Autowired
    @Qualifier("staticDao")//mysqlDao,staticDao
    private EmployeeDaoIntf employeeDaoIntf;

    private static final Logger log = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Override
    public EmployeesResponse getEmployees(int pageNum, int pageSize, String sortBy, String order) {
        List<Employee> employees;
        EmployeesResponse employeesResponse;
        Boolean hasMoreRecords;
        try{
            employees = employeeDaoIntf.getEmployees(pageNum, pageSize, sortBy, order);
            if(employees == null || employees.size() <1){
                employeesResponse =  new EmployeesResponse(new ArrayList<>(), false);
            }else {
                hasMoreRecords = employees.size() > pageSize;
                if (hasMoreRecords) {
                    employees = employees.subList(0, employees.size() - 1);
                }
                employeesResponse = new EmployeesResponse(employees, hasMoreRecords);
            }
        }catch (Exception e){
            log.error("Exception in getEmployees all ",e);
            throw e;
        }
        return employeesResponse;
    }

    @Override
    public Employee getEmployees(String employeeId) {

        Employee employee;
        try{
            employee = employeeDaoIntf.getEmployee(employeeId);
            if(employee == null){
                throw new EntityNotFoundException(EmployeeConstant.EMPLOYEE.getValue() +
                        EmployeeConstant.COLON_SEPARATOR.getValue()+EmployeeConstant.ALL.getValue(),
                        ExceptionConstants.EMPLOYEE_NOT_FOUND.getAppErrorCode(),
                        ExceptionConstants.EMPLOYEE_NOT_FOUND.getAppErrorMessage());
            }
        }catch (Exception e){
            log.error("Exception in getEmployees by id  ",e);
            throw e;
        }
        return employee;
    }
}
