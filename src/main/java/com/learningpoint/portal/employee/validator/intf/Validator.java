package com.learningpoint.portal.employee.validator.intf;

public interface Validator<E> {
    void validate(E e);
}
