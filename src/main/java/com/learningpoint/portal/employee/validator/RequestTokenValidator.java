package com.learningpoint.portal.employee.validator;

import com.learningpoint.portal.employee.exception.ValidationException;
import com.learningpoint.portal.employee.pojo.http.ExceptionConstants;
import com.learningpoint.portal.employee.validator.intf.Validator;

import java.util.Arrays;
import java.util.List;

public class RequestTokenValidator implements Validator<String> {

    private List<String> tokens;
    private ExceptionConstants exceptionConstants;

    public RequestTokenValidator(String tokenStr, String delimiter, ExceptionConstants exceptionConstants) {
            if(tokenStr!=null && tokenStr.contains(delimiter)){
                this.tokens = Arrays.asList(tokenStr.split(delimiter));
                this.exceptionConstants = exceptionConstants;
            }
    }

    @Override
    public void validate(String value) {
        if(!(tokens!=null && tokens.size() > 0 && tokens.contains(value))){
            throw new ValidationException(value,
                    exceptionConstants.getAppErrorCode(),
                    exceptionConstants.getAppErrorMessage());
        }
    }
}
