package com.learningpoint.portal.employee.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EmployeeUtil {

    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Calendar calobj = Calendar.getInstance();

    public static Date getDate(String dateStr){
        Date date;
        try {
             date = dateFormat.parse(dateStr);
        }catch (Exception e){
            date = Calendar.getInstance().getTime();
        }
        return date;
    }
}
