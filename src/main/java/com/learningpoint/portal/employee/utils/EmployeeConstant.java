package com.learningpoint.portal.employee.utils;

public enum EmployeeConstant {

    COLON_SEPARATOR(" : "),
    FORWARD_SLASH_SEPARATOR(" / "),
    EMPLOYEE("Employee"),
    ALL("All"),
    SORT_BY("firstName,lastName,hireDate,salary"),
    ORDER("ASC,DESC");

    private String value;

    EmployeeConstant(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
