package com.learningpoint.portal.employee.controller;

import com.learningpoint.portal.employee.pojo.Employee;
import com.learningpoint.portal.employee.pojo.EmployeesResponse;
import com.learningpoint.portal.employee.pojo.http.ExceptionConstants;
import com.learningpoint.portal.employee.service.intf.EmployeeServiceIntf;
import com.learningpoint.portal.employee.utils.EmployeeConstant;
import com.learningpoint.portal.employee.validator.RequestTokenValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/employees")
@Validated
public class EmployeeController {

    @Autowired
    private EmployeeServiceIntf employeeServiceIntf;

    private static final Logger log = LoggerFactory.getLogger(EmployeeController.class);

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmployeesResponse> getEmployees(@RequestParam(defaultValue = "1") @Min(1) Integer pageNum,
                                                          @RequestParam(defaultValue = "50") @Min(1) @Max(100) Integer pageSize,
                                                          @RequestParam(defaultValue = "firstName") String sortBy,
                                                          @RequestParam(defaultValue = "ASC") String order){
        EmployeesResponse employeesResponse;
        ResponseEntity<EmployeesResponse> responseEntity;
        try {
            validate(sortBy, order);

            employeesResponse = employeeServiceIntf.getEmployees(pageNum, pageSize, sortBy, order);
            responseEntity = new ResponseEntity<>(employeesResponse, HttpStatus.OK);
        }catch (Exception e){
            log.error("Exception in getEmployees ",e);
            throw e;
        }
        return responseEntity;
    }

    @GetMapping(value = "/{employeeId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployees(@PathVariable String employeeId){
        Employee employee;
        ResponseEntity<Employee> responseEntity;
        try {
            employee = employeeServiceIntf.getEmployees(employeeId);
            responseEntity = new ResponseEntity<>(employee, HttpStatus.OK);
        }catch (Exception e){
            log.error("Exception in getEmployees by employee id ",e);
            throw e;
        }
        return responseEntity;
    }

    private void validate(String sortBy, String order) {
        RequestTokenValidator requestTokenValidator;
        requestTokenValidator = new RequestTokenValidator(EmployeeConstant.SORT_BY.getValue(),",", ExceptionConstants.INVALID_REQUEST_SORT_BY_PARAMETER_FORMAT);
        requestTokenValidator.validate(sortBy);
        requestTokenValidator = new RequestTokenValidator(EmployeeConstant.ORDER.getValue(),",",ExceptionConstants.INVALID_REQUEST_ORDER_PARAMETER_FORMAT);
        requestTokenValidator.validate(order);
    }
}
